<?php  
    // defined('BASE_PATH') OR die('Permision Denied!');
    include_once('../bootstrap/init.php');
    
    if (!isAjaxRequest()) {
        die("Inavalid Request!");
    }

    if (!isset($_POST['action']) || empty($_POST['action'])) {
        die('Invalid Action');
    }

    switch ($_POST['action']) {
        case 'addFolder':
            if (!isset($_POST['folderName']) || strlen($_POST['folderName']) < 3) {
                die("نام فولدر باید بیش از 3 کاراکتر باشد!");
            }
            echo addFolder($_POST['folderName']);
            break;
        case 'addTask':
            $taskTitle = $_POST['addNewTask'];
            $folderId = $_POST['folderId'];
            if (!isset($taskTitle) || empty($taskTitle)) {
                die('عنوان تسک خود را وارد کنید!');
            }
            if (!isset($folderId) && is_numeric($folderId)) {
                die('یک فولدر انتخاب کنید!');
            }
            echo addNewTask($_POST['addNewTask'],$_POST['folderId']);
            break;
        case 'taskToggle':
            $taskId = $_POST['taskId'];
            if (!isset($taskId) && !is_numeric($taskId)) {
                die('ایدی تسک نامعتبر است!');
            }
            echo updateTaskStatus($taskId);
            break;
        default:
            die('Invalid Action!');
            break;
    }

?>