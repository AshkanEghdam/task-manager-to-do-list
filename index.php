<?php  
    include('bootstrap/init.php');
    if (isset($_GET['signout'])) {
        unset($_SESSION['login']);
    }

    if (!isLoggedIn()) {
        header("Location: auth.php");
    }

    
    if (isset($_GET['delete_folder_id']) && is_numeric($_GET['delete_folder_id'])) {
        deleteFolder($_GET['delete_folder_id']);
        header("Location: " . BASE_URL);
    }

    if (isset($_GET['delete_task_id']) && is_numeric($_GET['delete_task_id'])) {
        deleteTask($_GET['delete_task_id']);
    }

    $folder = getAllFolder();
    $tasks = getAllTasks();




    include_once("tpl/tpl-index.php");

?>