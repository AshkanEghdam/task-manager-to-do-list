<?php
    defined('BASE_PATH') OR die('Permision Denied!');

    function getCurrentUserId(){
        return 1;
    }

    function getAllFolder(){
        global $db_connection;
        $currentUser = getCurrentUserId();
        $query = "SELECT * FROM folder WHERE user_id = $currentUser";
        $stmt = $db_connection->prepare($query);
        $stmt->execute();
        $record = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $record;
    }

    function deleteFolder($folderId){
        global $db_connection;
        $query = "DELETE FROM folder WHERE id = :id";
        $stmt = $db_connection->prepare($query);
        $stmt->execute(['id' => $folderId]);
        $record = $stmt->rowCount();
        return $record;
    }

    function isAjaxRequest(){
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ) {
            return true;
        }
        return false;
    }

    function addFolder($folderName){
        global $db_connection;
        $userId = getCurrentUserId();
        $query = "INSERT INTO folder (name,user_id) VALUES (:name,:User_id)";
        $stmt = $db_connection->prepare($query);
        $stmt->execute([':name' => $folderName, ':User_id' => $userId]);
        $record = $stmt->rowCount();
        return $record;
    }
