<?php
    defined('BASE_PATH') OR die('Permision Denied!');

    function getAllTasks(){
        global $db_connection;
        // $folder = $_GET['folder_id'] ?? null;
        // $folderCondition = '';
        // if(isset($folder) and is_numeric($folder)){
        //     $folderCondition = " and folder_id=$folder";
        // }
        $folderId = $_GET['folder_id'] ?? null;
        $folderCondition = "";
        if (isset($folderId) && is_numeric($folderId)) {
            $folderCondition = " AND folder_id = $folderId";
        }
        $current_user_id = getCurrentUserId();
        $sql = "select * from tasks where user_id = $current_user_id  $folderCondition";
        $stmt = $db_connection->prepare($sql);
        $stmt->execute();
        $records = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $records;
    }

    function deleteTask($taskId){
        global $db_connection;
        
        $query = "DELETE FROM tasks WHERE id = :taskId";
        $stmt = $db_connection->prepare($query);
        $stmt->execute([':taskId' => $taskId]);
        $record = $stmt->rowCount();
        return $record;
    }

    function addNewTask($taskTitle,$folderId){
        global $db_connection;
        $currentUserId = getCurrentUserId();
        $query = "INSERT INTO tasks (title,user_id,folder_id) VALUES (:taskTitle,:user_id,:folder_id)";
        $stmt = $db_connection->prepare($query);
        $stmt->execute([':taskTitle' => $taskTitle , ':user_id'=> $currentUserId,'folder_id'=> $folderId ]);
        return $stmt->rowCount();
    }

    function updateTaskStatus($taskId){
        global $db_connection;
        $query = "UPDATE tasks SET is_done = 1 - is_done WHERE id = :taskId";
        $stmt = $db_connection->prepare($query);
        $stmt->execute([':taskId' => $taskId]);
        return $stmt->rowCount();
    }
