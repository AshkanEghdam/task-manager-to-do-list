<?php  
    defined('BASE_PATH') OR die('Permision Denied!');

    function isLoggedIn(){
        return isset($_SESSION['login']) ? true : false;
    }

    function getCurrentLoggedinUser(){
        return $_SESSION['login'] ?? null;
    }

    function registerUser($params){
        global $db_connection;
        $query = "INSERT INTO users (name, email, password) VALUES (:username, :email, :password)";
        $stmt = $db_connection->prepare($query);
        $stmt->execute([':username' => $params['username'], ':email' => $params['email'], ':password' => $params['password']]);
        return $stmt->rowCount() ? true : false;
    }
    function getCurretEmailAddress($email){
        global $db_connection;
        $query = "SELECT * FROM users WHERE email = :emailAddress";
        $stmt = $db_connection->prepare($query);
        $stmt->execute([':emailAddress' => $email]);
        $record = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $record[0] ?? null;
    }

    function loginUser($email, $password){
        $catchEmail = getCurretEmailAddress($email);
        if (is_null($catchEmail)) {
            return false;
        }else{
            if ($catchEmail->password == $password) {
                $_SESSION['login'] = $catchEmail;
                return true;
            }
        }
    }
?>