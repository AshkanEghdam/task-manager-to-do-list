<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title><?= SITE_TITLE?></title>
  <link rel="stylesheet" href="<?= BASE_URL ?>assets/css/style.css">

</head>
<body>
<!-- partial:index.partial.html -->
<div class="page">
  <div class="pageHeader">
    <div class="title">Dashboard</div>
    <div class="userPanel">
      <a href="<?= BASE_URL . '?signout=1' ?>"><i class="fa fa-sign-out"></i></a>
      <span class="username"><?= getCurrentLoggedinUser()->name; ?> </span>
      <img src="https://s3.amazonaws.com/uifaces/faces/twitter/kolage/73.jpg" width="40" height="40"/>
    </div>
  </div>
  <div class="main">
    <div class="nav">
      <div class="searchbox">
        <div><i class="fa fa-search"></i>
          <input type="search" placeholder="Search"/>
        </div>
      </div>
      <div class="menu">
        <div class="title">Navigation</div>
        <ul>
          <li class="active"> <i class="fa fa-tasks" ></i><a href="<?=BASE_URL?>">All Folders</a></li>
        <!-- show all folders in li tag -->
          <?php foreach ($folder as $folders): ?>
          <li> 
            <a href="?folder_id=<?= $folders->id?>"> <i class="fa fa-folder"></i><?= $folders->name ?> </a>
            <a href="?delete_folder_id=<?= $folders->id; ?>" class="remove" onclick="return confirm('Are you sure to delete <?= $folder->name; ?>');">x</a>
          </li>
          <?php endforeach;  ?>
          <div><i class=""></i>
            <input class="input" type="text" id="addNewFolderInput" placeholder="add new folder..."/>
            <button id="addNewFolderBtn" class="btn">+</button>
          </div>
        </ul>
      </div>
    </div>
    <div class="view">
      <div class="viewHeader">
        <div class="title">
        <input class="input" type="text" id="addNewtaskInput" placeholder="add new task...">
        <button id="addNewtaskBtn" class="btn">+</button>
        </div>
        <div class="functions">
          <div class="button active">Add New Task</div>
          <div class="button">Completed</div>
          <div class="button inverz"><i class="fa fa-trash-o"></i></div>
        </div>
      </div>
      <div class="content">
        <div class="list">
          <div class="title">Today</div>
          <ul>
          <?php if (sizeof($tasks) > 0): ?>
          <?php foreach ($tasks as $task): ?>
            <li class="<?= $task->is_done ? 'checked' : '';?>">
            <i data-taskId="<?= $task->id ?>" class="isDone <?= $task->is_done ? 'fa fa-check-square-o' : 'fa fa-square-o' ?>"></i>
            <span><?= $task->title; ?></span>
              <div class="info">
                <div class="button green"><?= $task->is_done ? "Done" :  "In progress"; ?></div><span>Cteated at <?=$task->created_at?></span>
                <a href="?delete_task_id=<?= $task->id; ?>" class="remove" onclick="return confirm('Are you sure to delete <?= $task->title; ?>');">x</a>
              </div>
            </li>
          <?php endforeach; ?>
          <?php else: ?>
          <li>no task here...</li>
          <?php endif; ?>
          </ul>
        </div>
        <div class="list">
          <div class="title">Tomorrow</div>
          <ul>
            <li><i class="fa fa-square-o"></i><span>Find front end developer</span>
              <div class="info"></div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- partial -->
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script  src="<?= BASE_URL ?> assets/js/script.js"></script>
  <script>
    $(document).ready(function(){
      $('#addNewFolderBtn').click(function(e){
        var input = $('input#addNewFolderInput');
        $.ajax({
          url : 'process/ajaxHandler.php',
          method : 'post',
          data : {action : 'addFolder', folderName : input.val()},
          success : function(response){
            if (response == '1') {
              location.reload();
            }else{
              alert(response);
            }
          }
        })
      });
      $('#addNewtaskInput').on('keypress',function(e) {
        if(e.which == 13) {
          var addNewTaskInput = $('#addNewtaskInput');
          $.ajax({
            url : 'process/ajaxHandler.php',
            method : 'post',
            data : {
              action : 'addTask' ,folderId : <?= $_GET['folder_id'] ?? 0 ?>, addNewTask : addNewTaskInput.val()
            },
            success : function(response){
              if (response == 1) {
                location.reload();
              }else{
                alert(response);
              }
            }
          })
        }
      });
      $('#addNewtaskInput').focus();

      $('.isDone').click(function(e){
        var tId = $(this).attr('data-taskId');
        $.ajax({
          url : 'process/ajaxHandler.php',
          method : 'post',
          data : {action: 'taskToggle', taskId: tId},
          success : function(response){
            if (response == '1') {
              location.reload();
            }else{
              alert(response);
            }
          }
        })
      });
    });

  </script>
</body>
</html>
