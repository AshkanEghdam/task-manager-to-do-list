<?php
    session_start();
    include('constant.php');
    include( BASE_PATH . 'bootstrap/config.php');
    try {
        $db_connection = new PDO("mysql:host=$database_config->host;dbname=$database_config->dbname", $database_config->user, $database_config->password);
        // set the PDO error mode to exception
        $db_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
    } catch(PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
    }
    include( BASE_PATH . 'libs/helper.php');
    include( BASE_PATH . 'libs/lib-auth.php');
    include( BASE_PATH . 'libs/lib-tasks.php');