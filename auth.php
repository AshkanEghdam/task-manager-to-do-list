<?php
    include"bootstrap/init.php";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $action = $_GET['action'];
        $params = $_POST;
        if ($action == 'register') {
            $result = registerUser($params);
            if (!$result) {
                echo "You are not registered!";
            }
        }elseif($action == 'login'){
            $result = loginUser($params['email'],$params['password']);
            if (!$result) {
                echo "You are not logged in!";
            }else{
                header("Location: http://localhost/taskmanager/");
            }
        }
    }


    include"tpl/tpl-auth.php";